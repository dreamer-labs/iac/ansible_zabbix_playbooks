# ansible_playbooks

## zabbix_deploy
Use to deploy zabbix development enviornmnet based to dldev environment

Requirements:

1.) Shell file with your openstack creds - You will need to download from the Openstack panel your opencred.sh file and source that before running the playbook
2.) Cloudflare username and API key for DNS entries

ROLES needed:

```bash
ansible_role_borg_client
ansible_role_borg_repo
ansible_role_dns_records
ansible_role_elasticsearch
ansible_role_galera
ansible_role_galera_holland
ansible_role_grafana
ansible_role_graylog
ansible_role_ha
ansible_role_haproxy
ansible_role_hardening
ansible_role_ha_test
ansible_role_import_template
ansible_role_keepalived
ansible_role_nginx
ansible_role_packages
ansible_role_provisioning
ansible_role_repositories
ansible_role_security
ansible_role_syslog
ansible_role_zabbix_agent
ansible_role_zabbix_alert_scripts
ansible_role_zabbix_server
ansible_role_zabbix_templates
```

Openstack file should look like this:

```bash
#!/usr/bin/env bash
# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
#
# *NOTE*: Using the 3 *Identity API* does not necessarily mean any other
# OpenStack API is version 3. For example, your cloud provider may implement
# Image API v1.1, Block Storage API v2, and Compute API v2.0. OS_AUTH_URL is
# only for the Identity API served through keystone.
export OS_AUTH_URL=
# With the addition of Keystone we have standardized on the term **project**
# as the entity that owns the resources.
export OS_PROJECT_ID=
export OS_PROJECT_NAME=
export OS_USER_DOMAIN_NAME=
if [ -z "$OS_USER_DOMAIN_NAME" ]; then unset OS_USER_DOMAIN_NAME; fi
export OS_PROJECT_DOMAIN_ID=
if [ -z "$OS_PROJECT_DOMAIN_ID" ]; then unset OS_PROJECT_DOMAIN_ID; fi
# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME
# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USERNAME=
# With Keystone you pass the keystone password.
#echo "Please enter your OpenStack Password for project $OS_PROJECT_NAME as user $OS_USERNAME: "
#read -sr OS_PASSWORD_INPUT
export OS_PASSWORD=
# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi
export OS_INTERFACE=
export OS_IDENTITY_API_VERSION=
